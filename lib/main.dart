import 'package:flutter/material.dart';
import 'package:tappi/value/select.dart';
import 'package:tappi/value/francese.dart';
import 'dart:async';



void main() {
  runApp(MaterialApp(
    home: SplashScreen(),
  ));
}

class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), onDoneLoading);
  }

  onDoneLoading() async {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => select()));
  }

 @override
  Widget build(BuildContext context) {
      
       return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
             
             
             Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Image.asset('assets/image/language/logo2.png'),
              ),

             Padding(
               padding: const EdgeInsets.only(top:8.0),
               child: Text("Effervescence Predictor", style: new TextStyle(decoration: TextDecoration.none,fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(0, 63, 116, 1),fontFamily: 'RobotoMono'),),
             ),


              
              
            ],
          ),
          

            Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              Padding(
               padding: const EdgeInsets.only(top: 8.0),
               child: Container(
                   // width: MediaQuery.of(context).size.width,
                    width: 350,
                    height: 75,
                    
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: ExactAssetImage('assets/image/language/logointro.png'),
                      ),
                    ),
                  ),
             )
               
              
              
              
              
              
              
            ],
          ),


            Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Image.asset('assets/image/language/bolle.png', width: 200, height: 200,),
              ),

               Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Image.asset('assets/image/language/bolle.png', width: 200, height: 200,),
              ),
               
               
              
              
              
              
              
              
            ],
          ),




        ],
      ),
    );
      
    }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Welcome to Homescreen', style: TextStyle(fontSize: 24.0),),
      ),
    );
  }
}
