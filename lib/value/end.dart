import 'package:flutter/material.dart';

import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter/services.dart';

import 'package:flutter/services.dart';
import 'package:tappi/value/francese.dart';
import 'package:tappi/value/francese.dart' as prefix0;
import 'package:tappi/value/francese2.dart';


class chiusura extends StatefulWidget {
  chiusura({this.valore1,this.valore2});
  final valore1, valore2;
  @override
  _chiusuraState createState() => new _chiusuraState();
}

class _chiusuraState extends State<chiusura> {
var valore1, valore2;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    valore2=widget.valore2.toStringAsFixed(1);
    valore1=widget.valore1.toStringAsFixed(1);


  }

   Future<void> send() async {
    final Email email = Email(
    body: "Requête d’information envoyée par un utilisateur de l’APP « Effervescence Predictor \n Pression de CO\u2082 à 12°C:  $valore1 \n Pression de CO\u2082 à 20°C : $valore2",
  subject: "Requête d’information",
  recipients: ['commercial@pedifrance.com'],
    );

    String platformResponse;

    try {
      print('provo');
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
      print('ok');
    } catch (error) {
      platformResponse = error.toString();
      print('no');
    }

    if (!mounted) return;

    print('bo');
  }

  

    




  @override
  Widget build(BuildContext context) {

    double c_width = MediaQuery.of(context).size.width*0.8;
    
  return Scaffold(
    resizeToAvoidBottomPadding: false,
       appBar: AppBar(
                backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                    Text("Effervescence Predictor")
                    
                    
                  ],
                ),
        ),
      
      body: Container(
              
              width: double.infinity,
              height: 900,
              decoration: BoxDecoration(
                color: Colors.white
              ),
              child: 
              Padding(
                padding: const EdgeInsets.only(top:60.0),
                child: Column(
                  children: <Widget>[

                           Container (
      padding: const EdgeInsets.all(16.0),
      width: c_width,
      child: new Column (
        children: <Widget>[
          new Text ("Application portant sur l'évaluation des pertes de CO\u2082 modélisées par Gérard LIGER-BELAIR sur la base des valeurs des perméabilités au CO\u2082 des différents joints de capsules couronne de la gamme PE.DI, fournies par le LNE (Laboratoire National d’Essais). Cette simulation est valable dans le cadre d’un sertissage conforme de la capsule couronne et pour un vieillissement en cave à 12°C.", style: new TextStyle(fontFamily: 'RobotoSingle',fontSize: 14), textAlign: TextAlign.justify),
        ],
      ),
    ),

                    
             
             Container (
      padding: const EdgeInsets.all(16.0),
      width: c_width,
      child: new Column (
        children: <Widget>[
          new Text ("Pour toute information technique et/ou commerciale, contactez notre œnologue à", style: new TextStyle(fontFamily: 'RobotoSingle',fontSize: 14), textAlign: TextAlign.justify),
        ],
      ),
    ),
     
      
      
      Padding(
        padding: const EdgeInsets.only(top:16.0),
        child: new RaisedButton(
                color: Color.fromRGBO(0, 63, 116, 1),
                onPressed: send,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Icon(Icons.share,color: Colors.white,),
                    Text(' '),
                    new Text('commercial@pedifrance.com  ', style: new TextStyle(fontSize: 16, fontFamily: 'RobotoSingle',color: Colors.white),),
                    
                  ],
                ),
     ),
      ),

             

              



          
          
          
        Expanded(
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: MaterialButton(
              onPressed: () => {},
              child: Padding(
                padding: const EdgeInsets.only(bottom:12.0),
                child: Container(
                      alignment: FractionalOffset.bottomCenter,
                    // width: MediaQuery.of(context).size.width,
                  width: 350,
                    height: 75,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: ExactAssetImage('assets/image/language/logointro.png'),
                        ),
                      ),
                    ),
              ),
            ),
          ),
        ),




                  ],
                ),
              ),
            ),
  );
   
      
     
    
   
    
    
  }
}

