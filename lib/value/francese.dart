import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import 'package:tappi/value/resultfr.dart';
import  'package:keyboard_actions/keyboard_actions.dart';

class calcfrancese extends StatefulWidget {
  calcfrancese({this.email});
  final String email;
  @override
  _calcfranceseState createState() => new _calcfranceseState();
}

class _calcfranceseState extends State<calcfrancese> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    phoneNumberFocusNode.addListener((){
      bool hasFocus=searchData.phoneNumberFocusNode.hasFocus;
      if(hasFocus)
        showOverlay(context);
      else
        removeOverlay();
    });

    _dropDownMenuItems = getDropDownMenuItems();
    linear = _dropDownMenuItems[2].value;

    _dropDownMenuItemssugar = getDropDownMenuItemssugar();
    sugar = _dropDownMenuItemssugar[9].value;


     _dropDownMenuItemsformat = getDropDownMenuItemsformat();
    format = _dropDownMenuItemsformat[0].value;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
  }

    List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }


     List<DropdownMenuItem<String>> getDropDownMenuItemssugar() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _sugar) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

 List<DropdownMenuItem<String>> getDropDownMenuItemsformat() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _format) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

  int _zucchero = 15;
  
  var sizeofbottle="37,5cL";
  int _radioValue = 0;

  var diametro="29mm";
  int _radioValue2 = 0;
 FocusNode _nodeText1 = FocusNode();
  FocusNode _nodeText2 = FocusNode();
  FocusNode _nodeText3 = FocusNode();
  FocusNode _nodeText4 = FocusNode();
  FocusNode _nodeText5 = FocusNode();

FocusNode phoneNumberFocusNode = new FocusNode();

  var anni="1";

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
}
  
  void _sizeofbottle(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          sizeofbottle = "37,5cL";
          break;
        case 1:
          sizeofbottle = "75cL";
          break;
        case 2:
          sizeofbottle = "150cL";
          

          break;
      }
    });
  }

    
  void _diameterbottle(int value2) {
    setState(() {
      _radioValue2 = value2;
  
      switch (_radioValue2) {
        case 0:
          diametro = "29mm";
          break;
        case 1:
          diametro = "26mm";
          break;
       
      }
    });
  }

  List _cities =
  ["TOP", "TOP+3", "TOP+", "TOP Z","TOP S",];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String linear;

   void changedDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      linear = selectedCity;
    });
}

List _sugar =
  ["15", "16", "17", "18","19","20", "21", "22", "23", "24", "25", "26"];

  List<DropdownMenuItem<String>> _dropDownMenuItemssugar;
  String sugar;

   void changedDropDownItemsugar(String sugarselect) {
    print("Selected city $sugarselect, we are going to refresh the UI");
    setState(() {
      sugar = sugarselect;
    });
}

List _format =
  ["Demi (37,5cL)", "Standard (75cL)", "Magnum (150cL)"];

  List<DropdownMenuItem<String>> _dropDownMenuItemsformat;
  String format="Demi (37,5cL)";

   void changedDropDownItemsformat(String formatselect) {
    print("Selected city $formatselect, we are going to refresh the UI");
    setState(() {
      format = formatselect;
    });
}

<<<<<<< HEAD
  
=======
KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardAction(
          focusNode: _nodeText1,
        ),
        KeyboardAction(
          focusNode: _nodeText2,
          closeWidget: Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.close),
          ),
        ),
        KeyboardAction(
          focusNode: _nodeText3,
          onTapAction: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Text("Custom Action"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("OK"),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    ],
                  );
                });
          },
        ),
        KeyboardAction(
          focusNode: _nodeText4,
          displayCloseWidget: false,
        ),
        KeyboardAction(
          focusNode: _nodeText5,
          closeWidget: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text("CLOSE"),
          ),
        ),
      ],
    );
  }


>>>>>>> master






  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomPadding:false,
 floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color.fromRGBO(0, 63, 116, 1),
        elevation: 2.0,
        icon: const Icon(Icons.navigate_next),
        label: const Text('Résultats'),
        onPressed: () {
          if (format=="Magnum (150cL)"){ //controllo non esce da qua!!! 
             
             if (diametro=="26mm"){
              showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('If size of bottle is 150ml the diamter is only 29mm',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold, fontFamily: 'RobotoMono' )), 
            
        
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Continue with 29mm',style: TextStyle(fontSize: 14),),
                onPressed:(){
                
                anni=anni.replaceAll(",", ".");
                Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => new resultfr(
                    linear: linear,
                    diametro: "29mm",
                    sizeofbottle: format,
                    zucchero: sugar,
                    anni: anni,
                  )));
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );
        }
        else{

            
            
            anni=anni.replaceAll(",", ".");
            print('********************************************* $format');
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => new resultfr(
                  linear: linear,
                  diametro: diametro,
                  sizeofbottle: format,
              // sizeofbottle: sizeofbottle,
                  zucchero: sugar,
                  anni: anni,
                )));
        }

          }
          else{
            

            anni=anni.replaceAll(",", ".");
            print(anni);
            

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => new resultfr(
                  linear: linear,
                  diametro: diametro,
                  sizeofbottle: format,
                  zucchero: sugar,
                  anni: anni,
                )));

          }
          

          



        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('    ')
             // IconButton(
               // icon: Icon(Icons.menu),
             //   onPressed: () {},
             // ),
             // IconButton(
              //  icon: Icon(Icons.search),
              //  onPressed: () {},
             // )
            ],
          ),
        ),
),

    

     appBar: AppBar(
                backgroundColor: Color.fromRGBO(0, 63, 116, 1), 
                title: Row(
                  children: <Widget>[
                    
                    Text("Calcul de pression")
                    
                    
                  ],
                ),
              ),
     body:
     
     
        
    
 

       Column(children: <Widget>[
           Padding(
             padding: const EdgeInsets.only(top:8.0),
             child: Container( 
              height: 140.0,
              width: double.infinity,
              decoration: BoxDecoration(

                image:DecorationImage( 
                 image: AssetImage(
                                "assets/image/language/banner.png"),
                                alignment: Alignment.center
                ),
                color: Colors.white
             
              ),
              child: Column(
               mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                 // Text('Demo', style: TextStyle(fontSize: 40,color: Colors.white),textAlign: TextAlign.left,),
                ],
              ),
              
          
          
          ),
           ),

         
         
         
         //qua inizia il blocco inserimento

        

          

           Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(' Sucre au tirage (g/L)  ', style: TextStyle(fontSize: 18,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                   ),

                    new DropdownButton(
                    value: sugar,
                    items: _dropDownMenuItemssugar,
                    onChanged: changedDropDownItemsugar,
                   )



       
        


                 ],),
               ),
             ),
           ),
         ),

           Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(' Format de la bouteille:', style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),),
                   ),

                    new DropdownButton(
                    value: format,
                    items: _dropDownMenuItemsformat,
                    onChanged: changedDropDownItemsformat,
                   )



       
        


                 ],),
               ),
             ),
           ),
         ),

         
        

         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Column(
                   children: <Widget>[
                     
                     Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Text(' Diamètre du goulot ', style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),),
                       ],
                     ),
                   
                     Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                      

                      new Radio( activeColor: Colors.black,
                                        value: 0,
                                        groupValue: _radioValue2,
                                        onChanged: _diameterbottle,
                                      ),
                                      new Text('29mm', style: TextStyle(fontSize: 18,color: Colors.black,fontFamily: 'RobotoMono'),),
                                      new Radio(
                                         activeColor: Colors.black,
                                        value: 1,
                                        groupValue: _radioValue2,
                                        onChanged: _diameterbottle,
                                      ),
                                      new Text('26mm ', style: TextStyle(fontSize: 18,color: Colors.black,fontFamily: 'RobotoMono'),),
                                      
                                     
        


                     ],),
                   ],
                 ),
               ),
             ),
           ),
         ),

         Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(' Choix du joint de capsule  ', style: TextStyle(fontSize: 18,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                   ),

                    new DropdownButton(
                    value: linear,
                    items: _dropDownMenuItems,
                    onChanged: changedDropDownItem,
                   )



       
        


                 ],),
               ),
             ),


             
           ),
         ),

          Padding(
           padding: const EdgeInsets.only(top:4.0),
           child: Container(
               decoration: new BoxDecoration(
                   
                 ),
             child: Padding(
               padding: const EdgeInsets.all(4.0),
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text(" Temps sur lattes (en années)", style: TextStyle(fontSize: 16,color: Colors.black, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                   ),

<<<<<<< HEAD
                    new FlatButton(
                      color: Color.fromRGBO(0, 63, 116, 1),
                      onPressed: (){

                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                               shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            contentPadding: EdgeInsets.all( 10.0),
                              
                              content:  new Row(
                               children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
=======



           Padding(
             padding: const EdgeInsets.only(top:2.0),
             child: Container(
                
                 decoration: new BoxDecoration(
                  color: Colors.white
                 ),
               child: new Padding(
                
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child:  new TextField(

>>>>>>> master
               keyboardType: TextInputType.number,
                focusNode: _nodeText1,

                 onChanged: (String str){
                      setState(() {
                       anni=str;        
                                      });
                    },
                  decoration: new InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                      filled: true,
                      hintStyle: new TextStyle(color: Colors.grey[800]),
                      hintText: "Temps sur lattes (en années)",
                      fillColor: Colors.white70),
                ),
                                  ),
                                ],
                              ),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("Retourner"),
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                ),
                                new FlatButton(
                                  child: new Text("Appliquer la valeur"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );



                      },
                      child: Text("Définir", style: TextStyle(fontSize: 14,color: Colors.white, fontWeight: FontWeight.bold,fontFamily: 'RobotoMono'),),
                    )



       
        


                 ],),
               ),
             ),


             
           ),
         ),






           

           

          




       ],)
     
   
      
     
    
   
    
    );
  }
}

