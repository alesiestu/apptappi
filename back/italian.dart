import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
class calcitalian extends StatefulWidget {
  calcitalian({this.email});
  final String email;
  @override
  _calcitalianState createState() => new _calcitalianState();
}

class _calcitalianState extends State<calcitalian> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _dropDownMenuItems = getDropDownMenuItems();
    _currentCity = _dropDownMenuItems[0].value;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
  }

    List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
 }

  int _zucchero = 15;
  
  var size;
  int _radioValue = 0;

  var diametro;
  int _radioValue2 = 0;


  var anni;
  
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          size = "pz";
          break;
        case 1:
          size = "kg";
          break;
        case 2:
          size = "lt";
          break;
      }
    });
  }

    
  void _handleRadioValueChange2(int value2) {
    setState(() {
      _radioValue2 = value2;
  
      switch (_radioValue) {
        case 0:
          diametro = "2";
          break;
        case 1:
          diametro = "3";
          break;
        case 2:
          diametro = "4";
          break;
      }
    });
  }

  List _cities =
  ["alessandro", "luca", "fate", "pippo",];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCity;

   void changedDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCity = selectedCity;
    });
}





  @override
  Widget build(BuildContext context) {
    
    return Material(

    
     child:
     
     
        
    
 

       Column(children: <Widget>[
           Container( 
            height: 216.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.purple,
              image: new DecorationImage(
              image: new AssetImage("assets/image/language/logo.png"),
              fit: BoxFit.none,
          ),
            ),
          
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
           Padding(
             padding: const EdgeInsets.only(top:150.0),
             child: Text('PREDISEZ LE DEVENIR DE VOS BULLES AVEC PE.DI' , textAlign: TextAlign.center, style: new TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.white),),
           )
            
          ],
          ),
          ),

         Padding(
           padding: const EdgeInsets.only(top:8.0),
           child: Container(
             decoration: new BoxDecoration(
                 color: Colors.blue,
               ),
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child:  Row(
                   
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[

                   Padding(
                     padding: const EdgeInsets.only(right:30.0),
                     child: Text('Quantità zucchero : ', style: TextStyle(fontSize: 18,color: Colors.white),),
                   ),

                    _zucchero!=0? new  IconButton(icon: new Icon(Icons.remove,color: Colors.white,),onPressed: ()=>setState(()=>_zucchero--),):new Container(),
                    new Text(_zucchero.toString(),style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,color: Colors.white)),
                    new IconButton(icon: new Icon(Icons.add,color: Colors.white,),onPressed: ()=>setState(()=>_zucchero++))
        


                 ],),
               
             ),
           ),
         ),

         
         Padding(
           padding: const EdgeInsets.only(top:8.0),
           child: Container(
             decoration: new BoxDecoration(
                 color: Colors.blue,
               ),
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[

                 Padding(
                   padding: const EdgeInsets.only(right:30.0),
                   child: Text('Taglia bottiglia : ', style: TextStyle(fontSize: 18,color: Colors.white),),
                 ),

                new Radio(
                   activeColor: Colors.white,
                                  value: 0,
                                  groupValue: _radioValue,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text('XS', style: TextStyle(fontSize: 18,color: Colors.white),),
                                new Radio(
                                  activeColor: Colors.white,
                                  value: 1,
                                  groupValue: _radioValue,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text('M', style: TextStyle(fontSize: 18,color: Colors.white),),
                                new Radio(
                                  activeColor: Colors.white,
                                  value: 2,
                                  groupValue: _radioValue,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text('L', style: TextStyle(fontSize: 18,color: Colors.white),),
        


               ],),
             ),
           ),
         ),

         Padding(
           padding: const EdgeInsets.only(top:8.0),
           child: Container(
               decoration: new BoxDecoration(
                   color: Colors.blue,
                 ),
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                 Padding(
                   padding: const EdgeInsets.only(right:10.0),
                   child: Text('Diametro: ', style: TextStyle(fontSize: 18, color: Colors.white),),
                 ),

                new Radio( activeColor: Colors.white,
                                  value: 0,
                                  groupValue: _radioValue2,
                                  onChanged: _handleRadioValueChange2,
                                ),
                                new Text('1', style: TextStyle(fontSize: 18,color: Colors.white),),
                                new Radio(
                                   activeColor: Colors.white,
                                  value: 1,
                                  groupValue: _radioValue2,
                                  onChanged: _handleRadioValueChange2,
                                ),
                                new Text('2', style: TextStyle(fontSize: 18,color: Colors.white),),
                                new Radio(
                                   activeColor: Colors.white,
                                  value: 2,
                                  groupValue: _radioValue2,
                                  onChanged: _handleRadioValueChange2,
                                ),
                                new Text('3', style: TextStyle(fontSize: 18,color: Colors.white),),
        


               ],),
             ),
           ),
         ),

         Padding(
           padding: const EdgeInsets.only(top:8.0),
           child: Container(
               decoration: new BoxDecoration(
                   color: Colors.blue,
                 ),
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[

                 Padding(
                   padding: const EdgeInsets.only(right:30.0),
                   child: Text('7 possibilità ', style: TextStyle(fontSize: 18,color: Colors.white),),
                 ),

                  new DropdownButton(
                  value: _currentCity,
                  items: _dropDownMenuItems,
                  onChanged: changedDropDownItem,
                 )



       
        


               ],),
             ),
           ),
         ),

           Padding(
             padding: const EdgeInsets.only(top:8.0),
             child: Container(
                 decoration: new BoxDecoration(
                   color: Colors.blue,
                 ),
               child: new Padding(
                
                padding: const EdgeInsets.all(16.0),
                child: TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (String str){
                    setState(() {
                     anni=str;        
                                    });
                  },
                  decoration: new InputDecoration(
                 
                    hintText: "Indica il numero di anni", 
                    

                  ),
                ),
          ),
             ),
           ),





       ],)
     
   
      
     
    
   
    
    );
  }
}

